﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using payers_api.Models;

namespace payers_api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PayersController : ControllerBase
    {
        private readonly PayerContext _context;    
        
        public PayersController(PayerContext context)         
        {
            _context = context; 
        }
        
        // GET api/payers
        [HttpGet]
        public IEnumerable<Payer> Get()
        {
            return _context.Payers;
        }

        // GET api/payers/5
        [HttpGet("{id}")]
         public async Task<IActionResult> Get([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var payer = await _context.Payers.SingleOrDefaultAsync(m => m.Id == id);

            if (payer == null)
                return NotFound();

            return Ok(payer);
        }

        // POST api/payers
        [HttpPost]
        public async Task<IActionResult> PostPayer([FromBody] Payer payer)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _context.Payers.Add(payer);
            try {
                await _context.SaveChangesAsync();
            }
            catch(Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There is already a payer with that name");
            }

            return CreatedAtAction("Get", new { id = payer.Id }, payer);
        }

        // PUT api/payers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPayer([FromRoute] int id, [FromBody] Payer payer)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != payer.Id)
                return BadRequest();

            _context.Entry(payer).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PayerExists(id))
                    return NotFound();
                throw;
            }
            catch(Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There is already a payer with that name");
            }

            return NoContent();
        }
        // DELETE api/payers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var payer = await _context.Payers.SingleOrDefaultAsync(m => m.Id == id);
            if (payer == null)
                return NotFound();

            _context.Payers.Remove(payer);
            await _context.SaveChangesAsync();

            return Ok(payer);
        }

        private bool PayerExists(int id)
        {
            return _context.Payers.Any(e => e.Id == id);
        }
    }
}
