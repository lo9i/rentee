﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using payers_api.Models;

namespace payers_api.Controllers
{
    [Produces("application/json")]
    [Route("api/branches")]
    [ApiController]
    public class PayerBranchesController : ControllerBase
    {
        private readonly PayerContext _context;    
        
        public PayerBranchesController(PayerContext context)         
        {
            _context = context; 
        }
        
        // GET api/payer/branches
        [HttpGet("for_payer/{payer_id}")]
        public IEnumerable<PayerBranch> GetBranches([FromRoute] int payer_id)
        {
            return _context.PayersBranches.Where(b=>b.Payer.Id == payer_id);
        }

        // GET api/branches/5
        [HttpGet("{id}")]
         public async Task<IActionResult> Get([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var branch = await _context.PayersBranches.SingleOrDefaultAsync(m => m.Id == id);

            if (branch == null)
                return NotFound();

            return Ok(branch);
        }

        // POST api/branches
        [HttpPost]
        public async Task<IActionResult> PostPayerBranch([FromBody] PayerBranch branch)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _context.PayersBranches.Add(branch);
            try {
                await _context.SaveChangesAsync();
            }
            catch(Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There is already a branch with that name");
            }

            return CreatedAtAction("Get", new { id = branch.Id }, branch);
        }

        // PUT api/branches/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPayerBranch([FromRoute] int id, [FromBody] PayerBranch branch)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != branch.Id)
                return BadRequest();

            _context.Entry(branch).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PayerBranchExists(id))
                    return NotFound();
                throw;
            }
            catch(Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There is already a branch with that name");
            }

            return NoContent();
        }
        // DELETE api/branches/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var branch = await _context.PayersBranches.SingleOrDefaultAsync(m => m.Id == id);
            if (branch == null)
                return NotFound();

            _context.PayersBranches.Remove(branch);
            await _context.SaveChangesAsync();

            return Ok(branch);
        }

        private bool PayerBranchExists(int id)
        {
            return _context.PayersBranches.Any(e => e.Id == id);
        }
    }
}
