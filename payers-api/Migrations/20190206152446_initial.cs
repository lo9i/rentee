﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace payers_api.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Payers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(maxLength: 450, nullable: false),
                    Code = table.Column<string>(maxLength: 450, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PayersBranches",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    FormattedAddress = table.Column<string>(nullable: false),
                    LattitudeLongitude = table.Column<string>(nullable: false),
                    PayerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayersBranches", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PayersBranches_Payers_PayerId",
                        column: x => x.PayerId,
                        principalTable: "Payers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Payers_Code",
                table: "Payers",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Payers_Name",
                table: "Payers",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PayersBranches_PayerId",
                table: "PayersBranches",
                column: "PayerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PayersBranches");

            migrationBuilder.DropTable(
                name: "Payers");
        }
    }
}
