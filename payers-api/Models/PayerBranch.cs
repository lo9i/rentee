using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace payers_api.Models {
public class PayerBranch
{
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }
    [Required]
    public string Code { get; set; }
    [Required]
    public string FormattedAddress { get; set; }
    [Required]
    public string LattitudeLongitude { get; set; }
    public int PayerId { get; set; }
    public Payer Payer { get; set; }
  }
}
