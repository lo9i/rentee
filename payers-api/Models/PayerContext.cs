using Microsoft.EntityFrameworkCore;  
namespace payers_api.Models {
    public class PayerContext : DbContext     
    {         
        public PayerContext(DbContextOptions<PayerContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payer>().HasIndex(u => u.Name).IsUnique();
            modelBuilder.Entity<Payer>().HasIndex(u => u.Code).IsUnique();
            modelBuilder.Entity<Payer>()
                .HasMany( p => p.Branches)
                .WithOne ( b=> b.Payer );
        }
        public DbSet<Payer> Payers { get; set; }
        public DbSet<PayerBranch> PayersBranches { get; set; }   
    }
}
