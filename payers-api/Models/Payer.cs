using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace payers_api.Models {
    public class Payer
    {

        public int Id { get; set; }
        
        [Required]
        [StringLength(450)]
        public string Name { get; set; }
        
        [Required]
        [StringLength(450)]
        public string Code { get; set; }

        public ICollection<PayerBranch> Branches { get; set; }
  }
}
