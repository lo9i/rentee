import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-add-update-branch',
  templateUrl: './add-update-branch.component.html',
  styleUrls: ['./add-update-branch.component.css']
})

export class AddUpdateBranchComponent implements OnInit {
    @Input() branchInfo: any;
    @Output() branchCreated = new EventEmitter<any>();
  
    public buttonText = 'Save';
  
    constructor() {
      this.clearBranchInfo();
    }
  
    ngOnInit() {
    }
  
    private clearBranchInfo = function () {
      // Create an empty branch object
      this.branchInfo = {
        id: undefined,
        payerid: -1,
        name: '',
        code: '',
        formattedAddress: '',
        lattitudeLongitude: ''
      };
    };
  
    public addUpdateBranchRecord = function (event) {
      this.branchCreated.emit(this.branchInfo);
      this.clearBranchInfo();
    };
  }
  