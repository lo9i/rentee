import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { PayersService } from './payers.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { DataTablesModule } from 'angular-datatables';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { HomeComponent } from './home/home.component';
import { AddUpdatePayerComponent } from './add-update-payer/add-update-payer.component';
import { GridPayersComponent } from './grid-payers/grid-payers.component';
import { NotificationService } from './notification.service';
import { ErrorsHandler } from './errors-handler';
import { ErrorsService } from './errors.service';
import { ServerErrorsInterceptor } from './server-errors.interceptor';
import { GridBranchesComponent } from './grid-branches/grid-branches.component';
import { AddUpdateBranchComponent } from './add-update-branch/add-update-branch.component';
import { BranchesService } from './branches.service';

@NgModule({
  declarations: [
    AppComponent,
    GridPayersComponent,
    AddUpdatePayerComponent,
    HomeComponent,
    GridBranchesComponent,
    AddUpdateBranchComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    DataTablesModule,
    AngularFontAwesomeModule,
  ],
  providers: [PayersService, BranchesService,
      NotificationService,
      ErrorsService,
      {
        provide: ErrorHandler,
        useClass: ErrorsHandler,
      },
      {
        provide: HTTP_INTERCEPTORS,
        useClass: ServerErrorsInterceptor,
        multi: true
      },],
  bootstrap: [AppComponent]
})
export class AppModule { }
