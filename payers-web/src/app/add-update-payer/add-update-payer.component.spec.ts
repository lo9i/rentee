import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpdatePayerComponent } from './add-update-payer.component';

describe('AddUpdatePayerComponent', () => {
  let component: AddUpdatePayerComponent;
  let fixture: ComponentFixture<AddUpdatePayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUpdatePayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpdatePayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
