import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-update-payer',
  templateUrl: './add-update-payer.component.html',
  styleUrls: ['./add-update-payer.component.css']
})


export class AddUpdatePayerComponent implements OnInit {
  @Input() payerInfo: any;
  @Output() payerCreated = new EventEmitter<any>();

  public buttonText = 'Save';

  constructor() {
    this.clearPayerInfo();
  }

  ngOnInit() {
  }

  private clearPayerInfo = function () {
    // Create an empty payer object
    this.payerInfo = {
      id: undefined,
      name: '',
      code: ''
    };
  };

  public addUpdatePayerRecord = function (event) {
    this.payerCreated.emit(this.payerInfo);
    this.clearPayerInfo();
  };
}
