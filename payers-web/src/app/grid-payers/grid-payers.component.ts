import { Component, OnInit, OnDestroy, ViewChild, EventEmitter, Output } from '@angular/core';
import { PayersService } from '../payers.service'
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import * as bootstrap from "bootstrap"
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-grid-payers',
  templateUrl: './grid-payers.component.html',
  styleUrls: ['./grid-payers.component.css'],
})

export class GridPayersComponent implements OnDestroy, OnInit {
  @Output() payerSelected = new EventEmitter<any>();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  public currentPayer: any;
  payersData: Array<any> = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  constructor(private payersService: PayersService) {
    this.currentPayer = this.setInitialValuesForPayerData();
    payersService.get().subscribe((data: any) => {
      this.payersData = data;
      this.dtTrigger.next();
    });
  }

  ngOnInit() { 

  }
  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  private setInitialValuesForPayerData() {
    return {
      id: undefined,
      name: '',
      code: ''
    }
  }

  public deleteRecord(record) {
    const deleteIndex = _.findIndex(this.payersData, { id: record.id });
    this.payersService.remove(record).subscribe(
      result => {
        this.payersData.splice(deleteIndex, 1);
        this.rerender();
      }
    );
  };

  public editRecord(record) {
    const clonedRecord = Object.assign({}, record);
    this.currentPayer = clonedRecord;
    $('#addUpdatePayerModal').modal('show');
  }

  public newRecord() {
    this.currentPayer = this.setInitialValuesForPayerData();
    $('#addUpdatePayerModal').modal('show');
  }

  public selectRecord(record) {
    this.payerSelected.emit(record);
  }

  public createUpdatePayer = function (payer: any) {
    $('#addUpdatePayerModal').modal('hide');
    if (payer.id == undefined)
      this.addPayer(payer)
    else
      this.editPayer(payer)
    this.currentPayer = this.setInitialValuesForPayerData();
  };

  private addPayer(payer: any) {
      this.payersService.add(payer).subscribe(
        payerRecord => {
          this.payersData.push(payerRecord);
          this.rerender();
        }
      );
  }

  private editPayer(payer: any) {
    let payerWithId = _.find(this.payersData, (el => el.id === payer.id));
    const updateIndex = _.findIndex(this.payersData, { id: payerWithId.id });
    this.payersService.update(payer).subscribe(
      payerRecord => {
        this.payersData.splice(updateIndex, 1, payer);
        this.rerender();
      }
    );
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
}
