import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridPayersComponent } from './grid-payers.component';

describe('GridPayersComponent', () => {
  let component: GridPayersComponent;
  let fixture: ComponentFixture<GridPayersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridPayersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridPayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
