import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  public payerId: any;
  constructor () {
  }
  ngOnInit() {
  }

  public selectPayer = function(payer: any) {
    this.payerId = payer.id;
  }
}
