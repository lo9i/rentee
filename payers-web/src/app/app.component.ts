import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NotificationService } from './notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  notification: string = 'aniabl';
  showNotification: boolean = true;

  constructor (private notificationService: NotificationService, private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.notificationService
            .notification$
            .subscribe(message => { 
              this.notification = message;
              this.showNotification = true;
              this.changeDetectorRef.detectChanges();
            });
  }
}
