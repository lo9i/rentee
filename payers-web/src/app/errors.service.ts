import { Injectable, Injector} from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router, Event, NavigationError } from '@angular/router';

import { Observable, of } from 'rxjs';


@Injectable()
export class ErrorsService {

  constructor(
    private injector: Injector,
    private router: Router,
  ) {
    // Subscribe to the NavigationError
    this.router
          .events  
          .subscribe((event: Event) => { 
            if (event instanceof NavigationError) {
                // Redirect to the ErrorComponent
                this.log(event.error)
                        .subscribe((errorWithContext) => { 
                          this.router.navigate(['/error'], { queryParams: errorWithContext })
                        });                
            }
          });
  }

  log(error) {
    // Log the error to the console
    let error_with_context = this.addContextInfo(error);
    console.error(error_with_context);
    // Send error to server
    return fakeHttpService.post(error_with_context);
  }

  addContextInfo(error) {
    // You can include context details here (usually coming from other services: UserService...)
    const name = error.name || null;
    const appId = 'shthppnsApp';
    const user = 'ShthppnsUser';
    const time = new Date().getTime();
    const id = `${appId}-${user}-${time}`;
    const location = this.injector.get(LocationStrategy);
    const url = location instanceof PathLocationStrategy ? location.path() : '';
    const status = error.status || null;
    const message = error.message || error.toString();
    const errorWithContext = {name, appId, user, time, id, url, status, message};
    return errorWithContext;
  }

}

class fakeHttpService {
  static post(error): Observable<any> {
    console.log('Error sent to the server: ', error);
    return of(error);
  }
}
