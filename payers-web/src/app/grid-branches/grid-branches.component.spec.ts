import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridBranchesComponent } from './grid-branches.component';

describe('GridBranchesComponent', () => {
  let component: GridBranchesComponent;
  let fixture: ComponentFixture<GridBranchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridBranchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridBranchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
