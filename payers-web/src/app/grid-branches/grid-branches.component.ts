import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { BranchesService } from '../branches.service'
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import * as bootstrap from "bootstrap"
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-grid-branches',
  templateUrl: './grid-branches.component.html',
  styleUrls: ['./grid-branches.component.css']
})
export class GridBranchesComponent implements OnDestroy, OnInit {
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    public _payerId: any;
    public currentBranch: any;
    branchesData: Array<any> = [];
    dtOptions: DataTables.Settings = {};
    dtTrigger = new Subject();
  
    constructor(private branchesService: BranchesService) {
      this.currentBranch = this.setInitialValuesForBranchData();
    }
  
    ngOnInit() { 
  
    }
    ngAfterViewInit(): void {
    }
  
    ngOnDestroy(): void {
      this.dtTrigger.unsubscribe();
    }
  
    private setInitialValuesForBranchData() {
      return {
        id: undefined,
        payerid: this._payerId,
        name: '',
        code: '',
        formattedAddress: '',
        lattitudeLongitude: ''
      }
    }
  
    get payerId(): any {
        return this._payerId;
    }
    
    @Input('payerId')
    set payerId(value: any) {
        this._payerId = value;
        if(this._payerId)
          this.branchesService.get(this._payerId).subscribe((data: any) => {
                                                                            this.branchesData = data;
                                                                            this.rerender();
                                                                          });
        else {
          this.branchesData = [];
          this.rerender();
        }
    }

    public deleteBranchRecord(record) {
      const deleteIndex = _.findIndex(this.branchesData, { id: record.id });
      this.branchesService.remove(record).subscribe(
        result => {
          this.branchesData.splice(deleteIndex, 1);
          this.rerender();
        }
      );
    };
  
    public editBranchRecord(record) {
      const clonedRecord = Object.assign({}, record);
      this.currentBranch = clonedRecord;
      $('#addUpdateBranchModal').modal('show');
    }
  
    public newRecord() {
      this.currentBranch = this.setInitialValuesForBranchData();
      $('#addUpdateBranchModal').modal('show');
    }
  
    public createUpdateBranch = function (branch: any) {
      $('#addUpdateBranchModal').modal('hide');
      if (branch.id == undefined)
        this.addBranch(branch)
      else
        this.editBranch(branch)
      this.currentBranch = this.setInitialValuesForBranchData();
    };
  
    private addBranch(branch: any) {
        this.branchesService.add(branch).subscribe(
          branchRecord => {
            this.branchesData.push(branchRecord);
            this.rerender();
          }
        );
    }
  
    private editBranch(branch: any) {
      let branchWithId = _.find(this.branchesData, (el => el.id === branch.id));
      const updateIndex = _.findIndex(this.branchesData, { id: branchWithId.id });
      this.branchesService.update(branch).subscribe(
        branchRecord => {
          this.branchesData.splice(updateIndex, 1, branch);
          this.rerender();
        }
      );
    }
  
    rerender(): void {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
  }
  